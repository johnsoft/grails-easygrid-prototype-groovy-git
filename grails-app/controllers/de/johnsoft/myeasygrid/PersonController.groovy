package de.johnsoft.myeasygrid

import org.grails.plugin.easygrid.Easygrid
import org.grails.plugin.easygrid.Filter
import java.text.SimpleDateFormat

@Easygrid
class PersonController {
    
     def personsGrid = {
        dataSourceType 'gorm'
        domainClass Person
        
        inlineEdit false
        enableFilter ture
        columns {
            id {
                type 'id'
            }
            firstName
            lastName
        }
        
    }

    def index() { }
}
