import grails.gorm.DetachedCriteria
import org.codehaus.groovy.grails.orm.hibernate.cfg.NamedCriteriaProxy
import de.johnsoft.myeasygrid.Person

class BootStrap {

    def init = { servletContext ->
         new Person(firstName: 'Sigurd', lastName: 'Eckermann').save(failOnError: true)
         new Person(firstName: 'Verena', lastName: 'John').save(failOnError: true)
    }
    def destroy = {
    }
}
