<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main">
        <title>My First Grid</title>
         <r:require modules="easygrid-jqgrid-dev,export"/>
    </head>
    <body>
        <r:script>
            function customShowFormat(cellvalue, options, rowObject) {
                return "<a href='${g.createLink(controller: "person", action: "show")}/" + cellvalue + "'>" + cellvalue + "</a> ";
            }
        </r:script>
        <grid:grid name="persons" jqgrid.width='600' columns.id.jqgrid.formatter='customShowFormat'
             addUrl="${g.createLink(controller: 'person',action: 'add')}"/>
        <!-- <grid:exportButton name="persons"/> -->
    </body>
    <jq:jquery>
        console.log(jQuery('#persons_table').jqGridMethod('setSelection', 1));
    </jq:jquery>
</html>
